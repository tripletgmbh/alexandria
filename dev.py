#!/usr/bin/env python
import sys
import subprocess
import shutil
import os
import os.path
from datetime import datetime


def abspath(*args):
    return os.path.abspath(os.path.join(os.path.dirname(__file__), *args))


default_config = {
}


def command_venv():
    """ initializes the virtualenv
    """
    if os.path.exists('venv'):
        return
    shell(['virtualenv', 'venv', '-p', 'python3'])
    shell(['pip', 'install', '--editable', '.[dev]'])

    #shell(['pip', 'install', '--requirement', 'requirements.txt'])
    #shell(['pip', 'install', 'pycodestyle'])


def command_build():
    """ builds the application
    """
    shell(['python', 'setup.py', 'sdist', 'bdist_wheel'])


def command_cli(*args):
    """ executes alexandrias cli
    """
    command_venv()
    shell(['python', '-m', 'alexandria.cli'] + list(args))


def command_exec(*args):
    """ runs a shell command, with the path set to include venv
    """
    command_venv()
    command_node_modules()
    shell(args, default_config)


def command_clean():
    """ cleans the project dir
    """
    for name in ['venv', 'alexandria.egg-info', 'dist', 'build', 'htmlcov']:
        if os.path.exists(name):
            shutil.rmtree(name)


def command_test(*args):
    """ run all the unittests
    """
    command_venv()
    shell(['pycodestyle', 'alexandria',
                '--ignore=E126,E127,E128,W503',
                '--max-line-length=90'])
    shell(['pytest',
                '--cov=alexandria',
                '--cov-report', 'html',
                '-vvvv', 'tests/'] + list(args))

def command_help():
    """ displays the help message
    """
    print("usage: build <command> [<args>...]")
    print()
    print("commands:")
    width = max(len(name) for name in commands.keys())
    for name, func in commands.items():
        print("  %s - %s" % (name.ljust(width), (func.__doc__ or '').strip()))


class ShellError(RuntimeError):
    pass


def shell(args, env={}):
    allenv = {}
    allenv.update(os.environ)
    allenv.update(env)

    allenv.update({
        'PYTHONPATH': os.pathsep.join([
            abspath(),
            os.environ.get('PYTHONPATH', '')
        ]),
        'PATH': os.pathsep.join([
                    abspath('venv', 'Scripts'),
                    abspath('venv', 'bin'),
                ] + [os.environ['PATH']])
    })

    exe = find_exe(args[0], allenv)

    try:
        subprocess.check_call([exe] + args[1:], env=allenv)
    except Exception as exc:
        raise ShellError(exc)


def find_exe(name, env):
    for p in env['PATH'].split(os.pathsep):
        if p.strip():
            for ext in env.get('PATHEXT', '').split(';'):
                fullpath = os.path.join(p, name + ext)
                if os.path.isfile(fullpath):
                    return fullpath
    raise Exception('no executable {} found'.format(name))



commands = dict((name[len('command_'):], func)
                for name, func in globals().items()
                if name.startswith('command_'))


def main(args):
    try:
        if not args or args[0] not in commands:
            command_help()
        else:
            commands[args[0]](*args[1:])
    except ShellError as err:
        print(err)
        sys.exit(1)


if __name__ == '__main__':
    main(sys.argv[1:])
