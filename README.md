# Alexandria

because this time, we'll have backups

# Examples

"Hello world" in raw

  alexandria database.alex '["yield", [["const", "Hello, World"]]]'

Simple table manipulations in SQL

  alexandria database.alex -p sql "create table persons (id any, first_name any, last_name any)"
  alexandria database.alex -p sql "create table books (id any, owner_id any, title any)"
  alexandria database.alex -p sql "insert into persons (id, first_name, last_name) values (0, 'Alice', 'Arkham')"
  alexandria database.alex -p sql "insert into books (id, owner_id, title) values (0, 0, 'Cryptonomicon')"
  alexandria database.alex -p sql "insert into books (id, owner_id, title) values (1, 0, 'Diamond Age')"
  alexandria database.alex -p sql "select first_name, last_name, title from persons, books where persons.id = books.owner_id"

Show what SQL does raw:

  alexandria database.alex -p sql -s "select first_name, last_name, title from persons, books where persons.id = books.owner_id"
