from setuptools import setup


setup(
    name='alexandria',
    version='dev',
    description="because this time, we'll have backups.",
    author='Philipp Benjamin Koeppchen',
    author_email='info@triplet.gmbh',
    url='https://triplet.gmbh/',
    packages=[
        'alexandria'
    ],
    install_requires=[
        'click==7.1.2',
        'tatsu==4.4.0',

    ],
    extras_require={
        'dev': [
            'pytest==5.4.3',
            'pytest-cov==2.10.0',
            'pycodestyle==2.6.0',
        ]
    },
    entry_points={
        "console_scripts": [
            "alexandria = alexandria.cli:main",
        ]
    }
)
