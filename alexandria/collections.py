import struct
import pickle


class Collection:
    """ A Table-like thing, only way more stupid

    :meta:
        meta information to be set and read by the user
    """

    def add(self, item):
        """ adds the item to the collection
        """
        raise NotImplementedError("must be overridden")

    def cursor(self):
        """ returns a cursor for reading and manipulation
        """
        raise NotImplementedError("must be overridden")


class Cursor:
    value = None
    has_value = False

    def advance(self):
        raise NotImplementedError("must be overridden")

    def remove(self):
        raise NotImplementedError("must be overridden")

    def replace(self, item):
        raise NotImplementedError("must be overridden")

    def seek(self, offset):
        raise NotImplementedError("must be overridden")


class FileCollection(Collection):
    """
    """

    @classmethod
    def create(cls, database, name, pagesize):
        with database.open_file(name) as fp:
            fp.truncate(0)
            fp.write(struct.pack('I', pagesize))
            fp.write(b'\x00' * (pagesize - 4))
            return cls(database, name, pagesize)

    @classmethod
    def load(cls, database, name):
        with database.open_file(name) as fp:
            pagesize, = struct.unpack('I', fp.read(4))
            return cls(database, name, pagesize)

    def __init__(self, database, filename, pagesize):
        self.database = database
        self.filename = filename
        self.pagesize = pagesize

    def _get_meta(self):
        with self.database.open_file(self.filename) as fp:
            fp.seek_set(4)
            return pickle.loads(fp.read(self.pagesize - 4))

    def _set_meta(self, value):
        with self.database.open_file(self.filename) as fp:
            fp.seek_set(4)
            data = pickle.dumps(value)
            data += b'\x00' * (self.pagesize - len(data) - 4)
            fp.write(data)

    meta = property(_get_meta, _set_meta)

    def add(self, item):
        with self.database.open_file(self.filename) as fp:
            fp.seek_backwards(0)
            data = b'\x01' + pickle.dumps(item)
            data += b'\x00' * (self.pagesize - len(data))
            fp.write(data)

    def cursor(self):
        return FileCollectionCursor(
                        self.database.open_file(self.filename),
                        self.pagesize)


class FileCollectionCursor(Cursor):
    def __init__(self, file, pagesize):
        self.pagesize = pagesize
        self.file = file
        self.file.seek_set(pagesize)
        self.advance()

    def __iter__(self):
        while self.has_value:
            yield self.value
            self.advance()

    def advance(self):
        data = self.file.read(self.pagesize)
        if not data:
            self.value = None
            self.has_value = False
        elif data[0:1] == b'\x00':
            self.advance()
        else:
            self.value = pickle.loads(data[1:])
            self.has_value = True

    def replace(self, item):
        data = b'\x01' + pickle.dumps(item)
        data += b'\x00' * (self.pagesize - len(data))
        self.file.seek_relative(-self.pagesize)
        self.file.write(data)

    def remove(self):
        self.file.seek_relative(-self.pagesize)
        self.file.write(b'\x00')
        self.file.seek_relative(self.pagesize - 1)

    def seek(self, offset):
        self.file.seek_set(self.pagesize * (1 + offset))
