import operator
from pathlib import Path

from .util import SeekableFile
from .collections import FileCollection


class Database:
    def __init__(self, path, objects):
        self.objects = dict(objects)
        self.path = Path(path)

    def execute(self, cmd):
        proc = Processor(self)
        proc.execute(cmd)
        return proc.result

    def open_file(self, name):
        return SeekableFile(self.path / name)


class Processor:

    def __init__(self, db):
        self.db = db
        self.result = []
        self.registers = {}
        self.cursors = {}

    def execute(self, cmd):
        name, *args = cmd
        func = getattr(self, f'execute_{name}')
        func(*args)

    def execute_create_collection(self, name, meta):
        """ Creates an empty collection

        :name: string
            Name of the collection
        :meta: any
            User defined data to annotate the collection
        """
        collection = FileCollection.create(self.db, name, 1024)
        collection.meta = self.calculate(meta)
        self.db.objects[name] = collection

    def execute_block(self, block):
        """ Sequentially executes all operations in the block
        """
        for cmd in block:
            self.execute(cmd)

    def execute_loop(self, condition, block):
        """ Repeats the sequence of operations in the block as long as the
            condition remains trueish.
        """
        while self.calculate(condition):
            self.execute_block(block)

    def execute_set_register(self, register, expr):
        """ Sets the register to the result of the expression
        """
        self.registers[register] = self.calculate(expr)

    def execute_append(self, collection, expr):
        self.db.objects[collection].add(self.calculate(expr))

    def execute_replace(self, cursor, expr):
        self.cursors[cursor].replace(self.calculate(expr))

    def execute_yield(self, expr):
        self.result.append(self.calculate(expr))

    def execute_if(self, expression, block):
        if self.calculate(expression):
            self.execute_block(block)

    def execute_remove(self, cursor):
        self.cursors[cursor].remove()

    def execute_set_cursor(self, cursor, collection):
        self.cursors[cursor] = self.db.objects[collection].cursor()

    def execute_advance_cursor(self, cursor):
        self.cursors[cursor].advance()

    def calculate(self, expr):
        type, *args = expr
        func = getattr(self, f'calculate_{type}')
        return func(*args)

    def calculate_func(self, name, args):
        return {
            'and': operator.and_,
            'or': operator.or_,
            '/': operator.truediv,
            '*': operator.mul,
            '+': operator.add,
            '-': operator.sub,
            '>=': operator.ge,
            '>': operator.gt,
            '<=': operator.le,
            '<': operator.lt,
            '=': operator.eq,
            '!=': operator.ne,
            '<>': operator.ne,
        }[name](*[self.calculate(item) for item in args])

    def calculate_tuple(self, args):
        return tuple(self.calculate(value) for value in args)

    def calculate_register(self, args):
        register, *into = args
        value = self.registers[register]

        for index in into:
            value = value[index]

        return value

    def calculate_cursor_value(self, cursor):
        return self.cursors[cursor].value

    def calculate_cursor_has_value(self, cursor):
        return self.cursors[cursor].has_value

    def calculate_const(self, value):
        return value
