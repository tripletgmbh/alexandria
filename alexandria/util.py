import os


class SeekableFile:

    def __init__(self, path):
        self._fp = os.open(path, os.O_RDWR | os.O_CREAT)

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        self.close()
        return False

    def truncate(self, size):
        os.truncate(self._fp, 0)

    def read(self, size):
        return os.read(self._fp, size)

    def write(self, data):
        os.write(self._fp, data)

    def close(self):
        os.close(self._fp)

    def seek_set(self, offset):
        os.lseek(self._fp, offset, os.SEEK_SET)

    def seek_relative(self, offset):
        os.lseek(self._fp, offset, os.SEEK_CUR)

    def seek_backwards(self, offset):
        os.lseek(self._fp, offset, os.SEEK_END)
