import tatsu

from functools import reduce

parser = tatsu.compile("""

Statement
    = SelectStatement
    | InsertStatement
    | UpdateStatement
    | DeleteStatement
    | CreateTableStatement $;

InsertStatement = 'insert' __ 'into' __ table:Identifier __
                    '(' _ fields:Fieldlist _ ')' __
                    'values' __
                    '(' _ values+:LiteralValue { _ ',' _ values+:LiteralValue } * _ ')'
                    ;

UpdateStatement = 'update' __ table:Identifier __ 'set' __ assignments:AssignmentList
                    [ __ 'where' __ condition:ValueExpression ]
                    ;

AssignmentList = lst+:Assignment { _ ',' _ lst+:Assignment } * ;

Assignment = name:Identifier _ '=' _ value:ValueExpression ;

DeleteStatement = 'delete' __ 'from' __ table:Identifier
                    [ __ 'where' __ condition:ValueExpression ]
                    ;

SelectStatement = 'select' __ projection:Projection
               __ 'from' __ tables:Tablelist
               [ __ 'where' __ condition:ValueExpression ]
               ;

CreateTableStatement = 'create' __ 'table' __ table:Identifier _
                        '(' _ cols:ColumnDefinitionList _ ')';

ColumnDefinitionList = lst+:ColumnDefinition { _ ',' _ lst+:ColumnDefinition } * ;

ColumnDefinition = name:Identifier __ 'any' ;

Projection = lst+:ValueExpression { _ ',' _ lst+:ValueExpression } * ;

Tablelist = lst+:AliasableTablename { _ ',' _ lst+:AliasableTablename } * ;

AliasableTablename = tablename:Identifier [ __ 'as' __ alias:Identifier ] ;

Fieldlist = lst+:Field { _ ',' _ lst+:Field } * ;

ValueExpression = OrExpression ;

OrExpression
    = operands+:AndExpression { __ ops+:'or' __ operands+:AndExpression } *
    ;

AndExpression
    = operands+:ComparisonExpression { __ ops+:'and' __ operands+:ComparisonExpression } *
    ;

ComparisonExpression
    = operands+:SumExpression {
        _ ops+:( '=' | '!=' | '<>' | '<=' | '>=' | '<' | '>' )
        _ operands+:SumExpression } *
    ;

SumExpression
    = operands+:MulExpression { _ ops+:( '+' | '-') _ operands:MulExpression } *
    ;

MulExpression
    = operands+:Term { _ ops+:( '*' | '/') _ operands+:Term } *
    ;

Term
    = Value
    | '(' _ ValueExpression _ ')'
    ;

Value
    = FieldValue
    | LiteralValue
    ;

FieldValue = Field ;

LiteralValue = Literal ;

Identifier
    = id:/[a-zA-Z_][a-zA-Z0-9_]*/
    | '"' id:/[^"]/ '"'
    ;

Field
    = table:Identifier '.' field:Identifier
    | field:Identifier
    ;

Literal
    = StringLiteral
    | NumberLiteral
    ;

StringLiteral = /'[^']*'/ ;

NumberLiteral = /[0-9]+/ ;

__ = /[ \n\t]+/ ;

_ = /[ \n\t]*/ ;

""")


class Semantics:
    def __init__(self, database):
        self.database = database

    def _get_col_indexes(self, tables):
        uniquenames = {}
        names = {}

        for table_index, (table, alias) in enumerate(tables):
            for col_index, c in enumerate(self.database.objects[table].meta):
                uniquenames.setdefault(c, []).append((table_index, col_index))
                names[(alias, c)] = (table_index, col_index)

        uniquenames = {(None, colname): indexes[0]
            for colname, indexes
            in uniquenames.items()
            if len(indexes) == 1}

        return {
            **uniquenames,
            **names,
        }

    def OrExpression(self, ast):
        return self._binary_expression(ast)

    def AndExpression(self, ast):
        return self._binary_expression(ast)

    def SumExpression(self, ast):
        return self._binary_expression(ast)

    def MulExpression(self, ast):
        return self._binary_expression(ast)

    def ComparisonExpression(self, ast):
        return self._binary_expression(ast)

    def _binary_expression(self, ast):
        operands = ast['operands']
        operators = ast['ops']

        expression = operands[0]

        for operator, operand in zip(operators or [], operands[1:]):
            expression = ('func', operator, [expression, operand])

        return expression

    def _get_calc(self, cols, value):
        type, *args = value
        if type == 'field':
            name, = args
            return ('register', cols[name])
        elif type == 'literal':
            value, = args
            return ('const', value)
        elif type == 'func':
            name, params = args
            return ('func', name, [self._get_calc(cols, item)
                                                    for item in params])
        else:
            raise Exception("omfg", value)

    def SelectStatement(self, ast):
        cols = self._get_col_indexes(ast['tables'])

        body = ('yield', ('tuple', [self._get_calc(cols, p)
                                            for p in ast['projection']]))

        if ast['condition']:
            cond = ast['condition']
            body = ('if', self._get_calc(cols, ast['condition']), [body])

        for index, (tablename, alias) in list(enumerate(ast['tables']))[::-1]:
            body = ('block', [
                ('set_cursor', index, tablename),
                ('loop', ('cursor_has_value', index), [
                    ('set_register', index, ('cursor_value', index)),
                    body,
                    ('advance_cursor', index),
                ]),
            ])

        return body

    def InsertStatement(self, ast):
        table = ast['table']

        if any(t not in [None, table] for t, _ in ast['fields']):
            raise SqlError("problem with insert field")

        fields = [name for _, name in ast['fields']]
        given_values = tuple(v for _, v in ast['values'])

        if len(given_values) != len(fields):
            raise SqlError("field list and values dont match")

        values = dict(zip(fields, given_values))

        return (
            'append',
            table,
            ('tuple', [('const', values.get(col, None))
                            for col in self.database.objects[table].meta]))

    def UpdateStatement(self, ast):
        table = ast['table']
        condition = ast['condition']
        assignments = dict(ast['assignments'])

        cols = self._get_col_indexes([(table, table)])

        body = ('replace', 0, ('tuple', [
            (self._get_calc(cols, assignments[col])
                if col in assignments
                else ('register', (0, index)))
            for index, col in enumerate(self.database.objects[table].meta)
        ]))

        if condition:
            body = ('if', self._get_calc(cols, condition), [body])

        return ('block', [
            ('set_cursor', 0, table),
            ('loop', ('cursor_has_value', 0), [
                ('set_register', 0, ('cursor_value', 0)),
                body,
                ('advance_cursor', 0),
            ])
        ])

    def DeleteStatement(self, ast):
        table = ast['table']
        cols = self._get_col_indexes([(table, table)])

        body = ('remove', 0)

        if ast['condition']:
            body = ('if', self._get_calc(cols, ast['condition']), [body])

        return ('block', [
            ('set_cursor', 0, table),
            ('loop', ('cursor_has_value', 0), [
                ('set_register', 0, ('cursor_value', 0)),
                body,
                ('advance_cursor', 0),
            ]),
        ])

    def CreateTableStatement(self, ast):
        return ('create_collection',
                    ast['table'],
                    ('const', [name for name, _ in ast['cols']]))

    def AliasableTablename(self, ast):
        return (ast['tablename'], ast.get('alias', ast['tablename']))

    def StringLiteral(self, ast):
        return ast[1:-1]

    def NumberLiteral(self, ast):
        return int(ast)

    def ColumnDefinition(self, ast):
        return (ast['name'], 'any')

    def ColumnDefinitionList(self, ast):
        return ast['lst']

    def AssignmentList(self, ast):
        return ast['lst']

    def Assignment(self, ast):
        return (ast['name'], ast['value'])

    def Fieldlist(self, ast):
        return ast['lst']

    def Projection(self, ast):
        return ast['lst']

    def Tablelist(self, ast):
        return ast['lst']

    def Field(self, ast):
        return (ast['table'], ast['field'])

    def LiteralValue(self, ast):
        return ('literal', ast)

    def FieldValue(self, ast):
        return ('field', ast)

    def Identifier(self, ast):
        return ast['id']


def parse(text, database):
    return parser.parse(text, semantics=Semantics(database))


class SqlError(RuntimeError):
    pass
