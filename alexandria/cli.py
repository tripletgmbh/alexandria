import click
import json
import pickle
import os

from . import sql
from .database import Database

from pathlib import Path


@click.command()
@click.argument('database_path', type=click.Path())
@click.option('--preprocessor', '-p',
                    default='raw',
                    type=click.Choice(['raw', 'sql']),
                    help="Applies a preprocessor to the commands")
@click.option('--show-raw', '-s',
                    help="Only show raw result of the preprocessed command",
                    is_flag=True)
@click.argument('command')
def main(database_path, preprocessor, command, show_raw):
    database_path = Path(database_path)
    manifest_path = database_path / 'manifest'

    os.makedirs(database_path, exist_ok=True)

    if manifest_path.exists():
        with open(str(manifest_path), 'rb') as file:
            db = pickle.load(file)
    else:
        db = Database(database_path, {})

    try:
        raw = preprocessors[preprocessor](command, db)
    except Exception as exc:
        print(f"error while preprocessing:\n{exc}")
    else:
        if show_raw:
            print(json.dumps(raw, indent=2))
        else:
            for line in db.execute(raw):
                print(line)

            with open(manifest_path, 'wb') as file:
                pickle.dump(db, file)


preprocessors = {
    'sql': sql.parse,
    'raw': lambda text, db: json.loads(text),
}


if __name__ == '__main__':
    main()
