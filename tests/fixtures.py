import pytest
from alexandria import Database, FileCollection


@pytest.fixture
def db():
    database = Database('/tmp', {})

    database.objects['persons'] = table(
        database,
        'persons',
        ('id', 'parent_id', 'first_name', 'last_name'),
        [
            (0, None, 'alice', 'arkham'),
            (1, None, 'bob', 'baumeister'),
            (2, 1, 'charlotte', 'cherry'),
            (3, 1 , 'derrick', 'dalton'),
        ]
    )

    database.objects['books'] = table(
        database,
        'books',
        ('id', 'person_id', 'title'),
        [
            (0, 0, 'Unspeakable Horrors'),
            (1, 0, 'Necronomicon'),
            (2, 1, "Builder's Handbook"),
        ],
    )

    database.objects['tags'] = table(
        database,
        'tags',
        ('id', 'book_id', 'text'),
        [
            (0, 0, 'eldritch'),
            (1, 0, 'fiction'),
            (2, 1, 'eldritch'),
            (3, 1, 'manual'),
            (4, 2, 'manual'),
        ]
    )

    return database


def table(database, name, meta, rows):
    collection = FileCollection.create(database, name, 1024)
    collection.meta = meta
    for row in rows:
        collection.add(row)
    return collection
