from alexandria.sql import parse
from .fixtures import db


def test_create_table(db):
    db.execute(parse('create table enemies (name any)', db))

    result = db.execute(parse('select name from enemies', db))
    assert result == []


def test_select_all(db):
    query = parse('select first_name, last_name from persons', db)

    assert db.execute(query) == [
        ('alice', 'arkham'),
        ('bob', 'baumeister'),
        ('charlotte', 'cherry'),
        ('derrick', 'dalton'),
    ]


def test_select_filter(db):
    query = parse("""
        select first_name, last_name
        from persons
        where first_name = 'alice'
    """, db)

    assert db.execute(query) == [
        ('alice', 'arkham'),
    ]


def test_list_cross(db):
    query = parse("""
        select
            first_name,
            last_name,
            title
        from
            persons,
            books
        where persons.id = books.person_id
    """, db)

    assert db.execute(query) == [
        ('alice', 'arkham', 'Unspeakable Horrors'),
        ('alice', 'arkham', 'Necronomicon'),
        ('bob', 'baumeister', "Builder's Handbook"),
    ]


def test_list_triple_cross(db):
    query = parse("""
        select
            title,
            tags.text
        from
            persons,
            books,
            tags
        where persons.first_name = 'alice'
          and persons.id = books.person_id
          and books.id = tags.book_id
    """, db)

    assert db.execute(query) == [
        ('Unspeakable Horrors', 'eldritch'),
        ('Unspeakable Horrors', 'fiction'),
        ('Necronomicon', 'eldritch'),
        ('Necronomicon', 'manual'),
    ]


def test_list_alias(db):
    query = parse("""
        select
            parent.first_name,
            child.first_name
        from
            persons as parent,
            persons as child
        where
            parent.id = child.parent_id
    """, db)

    assert db.execute(query) == [
        ('bob', 'charlotte'),
        ('bob', 'derrick'),
    ]


def test_insert(db):
    db.execute(parse('''
        insert into persons (id, first_name, last_name)
        values (4, 'elias', 'eco')
    ''', db))

    query = parse('''
        select parent_id, first_name, last_name
        from persons
        where id = 4
    ''', db)

    assert db.execute(query) == [
        (None, 'elias', 'eco')
    ]


def test_delete(db):
    db.execute(parse("delete from persons where first_name = 'alice'", db))

    query = parse('select first_name, last_name from persons', db)
    assert db.execute(query) == [
            ('bob', 'baumeister'),
            ('charlotte', 'cherry'),
            ('derrick', 'dalton'),
    ]


def test_update(db):
    db.execute(parse("""
        update persons
        set first_name = 'Alice',
            last_name = last_name + '*'
        where first_name = 'alice'
    """, db))

    query = parse('select id, first_name, last_name from persons', db)
    assert db.execute(query) == [
            (0, 'Alice', 'arkham*'),
            (1, 'bob', 'baumeister'),
            (2, 'charlotte', 'cherry'),
            (3, 'derrick', 'dalton'),
    ]
