from .fixtures import db
from alexandria.database import Processor


def test_yield(db):
    proc = Processor(db)
    proc.execute(('yield', ('const', 'Hello, World!')))
    assert proc.result == ['Hello, World!']


def test_count(db):
    proc = Processor(db)
    proc.execute(('block', [
        ('set_register', 0, ('const', 1)),
        ('set_register', 0, ('func', '+', [('register', (0,)), ('const', 1)])),
        ('set_register', 0, ('func', '+', [('register', (0,)), ('const', 1)])),
        ('yield', ('register', (0,))),
    ]))
    assert proc.result == [3]


def test_loop(db):
    proc = Processor(db)
    proc.execute(('block', [
        ('set_register', 0, ('const', 0)),  # counter
        ('set_register', 1, ('const', 1)),  # accumulator

        ('loop', ('func', '<', [('register', (0,)), ('const', 10)]), [
            ('set_register', 0, ('func', '+',
                                    [('register', (0,)), ('const', 1)])),

            ('set_register', 1, ('func', '*',
                                    [('const', 2), ('register', (1,))]))
        ]),
        ('yield', ('register', (1,))),
    ]))
    assert proc.result == [1024]


def test_cursor(db):
    proc = Processor(db)
    proc.execute(('block', [
        ('set_cursor', 0, 'books'),
        ('loop', ('cursor_has_value', 0), [
            ('set_register', 0, ('cursor_value', 0)),
            ('yield', ('register', (0, 2))),
            ('advance_cursor', 0),
        ])
    ]))
    assert proc.result == [
        'Unspeakable Horrors',
        'Necronomicon',
        "Builder's Handbook",
    ]


def test_cursor_replace(db):
    proc = Processor(db)
    proc.execute(('block', [
        ('set_cursor', 0, 'books'),
        ('replace', 0, ('tuple', [
            ('const', 0),
            ('const', 0),
            ('const', 'Unspeakable Horros of the Void'),
        ]))
    ]))

    list(db.objects['books'].cursor())[0] == (0, 0, 'Unspeakable Horros of the Void')
