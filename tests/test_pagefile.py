import pytest

from alexandria.database import FileCollection, Database


@pytest.fixture
def filecollection():
    database = Database('/tmp', {})

    collection = FileCollection.create(database, 'xxx', 1024)
    collection.meta = '4thwall'
    collection.add('Hello, World!')
    collection.add('Hola, Mundo!')
    collection.add('Salut, Monde!')
    return FileCollection.load(database, 'xxx')


def test_meta(filecollection):
    assert filecollection.meta == '4thwall'


def test_iterate(filecollection):
    cursor = filecollection.cursor()
    assert list(cursor) == [
        'Hello, World!',
        'Hola, Mundo!',
        'Salut, Monde!',
    ]


def test_replace(filecollection):
    cursor = filecollection.cursor()
    cursor.advance()
    cursor.replace('¡Hola, Mundo!')

    cursor = filecollection.cursor()
    assert list(cursor) == [
        'Hello, World!',
        '¡Hola, Mundo!',
        'Salut, Monde!',
    ]


def test_remove(filecollection):
    cursor = filecollection.cursor()
    cursor.advance()
    cursor.remove()

    cursor = filecollection.cursor()
    assert list(cursor) == [
        'Hello, World!',
        'Salut, Monde!',
    ]
