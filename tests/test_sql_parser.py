from alexandria.sql import parse
from .fixtures import db


def test_simple_select(db):
    raw = parse('select first_name, persons.last_name from persons', db)
    assert raw == ('block', [
        ('set_cursor', 0, 'persons'),
        ('loop', ('cursor_has_value', 0), [
            ('set_register', 0, ('cursor_value', 0)),
            ('yield', ('tuple', [('register', (0, 2)), ('register', (0, 3))])),
            ('advance_cursor', 0),
        ]),
    ])


def test_conditional_select(db):
    raw = parse("select first_name, last_name from persons where first_name = 'alice'", db)

    assert raw == ('block', [
        ('set_cursor', 0, 'persons'),
        ('loop', ('cursor_has_value', 0), [
            ('set_register', 0, ('cursor_value', 0)),
            ('if',
                ('func', '=', [('register', (0, 2)), ('const', 'alice')]),
                [
                    ('yield', ('tuple', [('register', (0, 2)), ('register', (0, 3))]))
                ]),
            ('advance_cursor', 0),
        ]),
    ])


def test_complex_conditional_select(db):
    query = """
        select first_name, last_name
        from persons
        where first_name = 'alice'
           or id > 2 and id + 1 < 4
    """
    assert parse(query, db) == ('block', [
        ('set_cursor', 0, 'persons'),
        ('loop', ('cursor_has_value', 0), [
            ('set_register', 0, ('cursor_value', 0)),
            ('if', ('func', 'or', [
                ('func', '=', [('register', (0, 2)), ('const', 'alice')]),
                ('func', 'and', [
                    ('func', '>', [
                        ('register', (0, 0)),
                        ('const', 2)]),
                    ('func', '<', [
                        ('func', '+', [
                            ('register', (0, 0)),
                            ('const', 1)]),
                        ('const', 4)]),
                ])]),
                [
                    ('yield', ('tuple', [('register', (0, 2)), ('register', (0, 3))]))
                ]),
            ('advance_cursor', 0),
        ]),
    ])


def test_cross_select(db):
    raw = parse('select first_name, last_name from persons, books', db)
    assert raw == ('block', [
        ('set_cursor', 0, 'persons'),
        ('loop', ('cursor_has_value', 0), [
            ('set_register', 0, ('cursor_value', 0)),
            ('block', [
                ('set_cursor', 1, 'books'),
                ('loop', ('cursor_has_value', 1), [
                    ('set_register', 1, ('cursor_value', 1)),
                    ('yield', ('tuple', [('register', (0, 2)), ('register', (0, 3))])),
                    ('advance_cursor', 1),
                ]),
            ]),
            ('advance_cursor', 0),
        ]),
    ])


def test_simple_update(db):
    raw = parse("update persons set first_name = 'redacted'", db)
    assert raw == ('block', [
        ('set_cursor', 0, 'persons'),
        ('loop', ('cursor_has_value', 0), [
            ('set_register', 0, ('cursor_value', 0)),
            ('replace', 0, ('tuple', [
                ('register', (0, 0)),
                ('register', (0, 1)),
                ('const', 'redacted'),
                ('register', (0, 3)),
            ])),
            ('advance_cursor', 0),
        ])
    ])

def test_conditional_update(db):
    raw = parse("""
        update persons
        set first_name = first_name + '*'
        where first_name = 'alice'
    """, db)

    assert raw == ('block', [
        ('set_cursor', 0, 'persons'),
        ('loop', ('cursor_has_value', 0), [
            ('set_register', 0, ('cursor_value', 0)),
            ('if', ('func', '=', [('register', (0, 2)), ('const', 'alice')]), [
                ('replace', 0, ('tuple', [
                    ('register', (0, 0)),
                    ('register', (0, 1)),
                    ('func', '+', [
                        ('register', (0, 2)),
                        ('const', '*'),
                    ]),
                    ('register', (0, 3)),
                ])),
            ]),
            ('advance_cursor', 0),
        ]),
    ])


def test_insert(db):
    query = parse("""
        insert into books (id, title, person_id) values (4, 1, 'Alice in Wonderland')
    """, db)
    assert query == ('append', 'books', ('tuple', [
                                            ('const', 4),
                                            ('const', 'Alice in Wonderland'),
                                            ('const', 1)]))


def test_simple_delete(db):
    raw = parse("""
        delete from books
    """, db)

    assert raw == ('block', [
        ('set_cursor', 0, 'books'),
        ('loop', ('cursor_has_value', 0), [
            ('set_register', 0, ('cursor_value', 0)),
            ('remove', 0),
            ('advance_cursor', 0),
        ]),
    ])


def test_conditional_delete(db):
    raw = parse("delete from books where id = 1", db)

    assert raw == ('block', [
        ('set_cursor', 0, 'books'),
        ('loop', ('cursor_has_value', 0), [
            ('set_register', 0, ('cursor_value', 0)),
            ('if', ('func', '=', [('register', (0, 0)), ('const', 1)]), [
                ('remove', 0),
            ]),
            ('advance_cursor', 0),
        ]),
    ])


def test_parse_create_table(db):
    query = parse('create table enemies (name any)', db)
    assert query == ('create_collection', 'enemies', ('const', ['name']))


def test_operator_precedence_and_associativity(db):
    query = '''
        select
            1 + 2 + 3 - 4,
            1 * 2 + 3 * 4,
            1 and 2 and 3 and 4,
            1 and 2 or 3 and 4
        from
            persons
    '''
    assert parse(query, db) == ('block', [
        ('set_cursor', 0, 'persons'),
        ('loop', ('cursor_has_value', 0), [
            ('set_register', 0, ('cursor_value', 0)),
            ('yield', ('tuple', [
                ('func', '-', [
                    ('func', '+', [
                        ('func', '+', [
                            ('const', 1),
                            ('const', 2),
                        ]),
                        ('const', 3),
                    ]),
                    ('const', 4),
                ]),
                ('func', '+', [
                    ('func', '*', [
                        ('const', 1),
                        ('const', 2),
                    ]),
                    ('func', '*', [
                        ('const', 3),
                        ('const', 4),
                    ])
                ]),
                ('func', 'and', [
                    ('func', 'and', [
                        ('func', 'and', [
                            ('const', 1),
                            ('const', 2),
                        ]),
                        ('const', 3),
                    ]),
                    ('const', 4),
                ]),
                ('func', 'or', [
                    ('func', 'and', [
                        ('const', 1),
                        ('const', 2),
                    ]),
                    ('func', 'and', [
                        ('const', 3),
                        ('const', 4),
                    ]),
                ]),
            ])),
            ('advance_cursor', 0),
        ]),
    ])
